var customersObtenidos;
function getCustomers(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request=new XMLHttpRequest();
  request.onreadystatechange=function(){
     if(this.readyState==4 && this.status==200){
       //console.log(request.responseText);
       customersObtenidos=request.responseText;
       procesarCustomers();
     }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarCustomers(){
  var jsonCustomer=JSON.parse(customersObtenidos);
  var divTabla= document.getElementById("divTabla");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  //alert(jsonProductos.value[0].ProductName);
  for (var i = 0; i < jsonCustomer.value.length; i++) {
  //  console.log(jsonProductos.value[i].ProductName);
    var nuevaFIla=document.createElement("tr");

    var columnaID=document.createElement("td");
    columnaID.innerText=jsonCustomer.value[i].CustomerID;

    var columnaNOmbre=document.createElement("td");
    columnaNOmbre.innerText=jsonCustomer.value[i].ContactName;

    //var columnaBandera=document.createElement("td");
    var columnaBandera=document.createElement("img");
    var country=jsonCustomer.value[i].Country;
    if(country=="UK"){
      country="United-Kingdom";
    }
    columnaBandera.src="https://www.countries-ofthe-world.com/flags-normal/flag-of-"+country+".png";
    columnaBandera.classList.add("flag");
    nuevaFIla.appendChild(columnaID);
    nuevaFIla.appendChild(columnaNOmbre);
      nuevaFIla.appendChild(columnaBandera);
        tbody.appendChild(nuevaFIla);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
