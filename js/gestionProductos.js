var productosObtenidos;
function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request=new XMLHttpRequest();
  request.onreadystatechange=function(){
     if(this.readyState==4 && this.status==200){
       console.log(request.responseText);
       productosObtenidos=request.responseText;
       procesarProductos();
     }
  }
  request.open("GET",url,true);
  request.send();
}
function procesarProductos(){
  var jsonProductos=JSON.parse(productosObtenidos);
  var divTabla= document.getElementById("divTabCustomer");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  //alert(jsonProductos.value[0].ProductName);
  for (var i = 0; i < jsonProductos.value.length; i++) {
  //  console.log(jsonProductos.value[i].ProductName);
    var nuevaFIla=document.createElement("tr");
    var columnaNOmbre=document.createElement("td");
    columnaNOmbre.innerText=jsonProductos.value[i].ProductName;

    var columnaPrecio=document.createElement("td");
    columnaPrecio.innerText=jsonProductos.value[i].UnitPrice;

    var columnaStock=document.createElement("td");
    columnaStock.innerText=jsonProductos.value[i].UnitsInStock;

    nuevaFIla.appendChild(columnaNOmbre);
      nuevaFIla.appendChild(columnaPrecio);
        nuevaFIla.appendChild(columnaStock);
        tbody.appendChild(nuevaFIla);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
